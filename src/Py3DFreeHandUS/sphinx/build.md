# Build (developers only)

## MinGW compiler

A few parts of this library were veloped with Cython. Hence, a compiler is necessary.
MinGW64 is the choice when Miniconda is used as Python environment.

- downalod and install [MinGW64](http://mingw-w64.org/doku.php/download);
version 4.8.0 was tested.

- check that you have the folder *C:\MinGW64\bin*

- add *C:\MinGW64\bin* to the *PATH*

- open Windows console and type `gcc --version`; you should see something similar to:

```
gcc (rubenvb-4.8.0) 4.8.0
Copyright (C) 2013 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```
