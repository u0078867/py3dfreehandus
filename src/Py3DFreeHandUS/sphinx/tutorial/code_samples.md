# Code/data samples

Examples are provided in the folder *samples*.

### Non-versioned method

This is the easiest method. However, if the user wants to download only the
updates files, (s)he has to know it in advance. The versioned method allows for
this.

For the non-versioned method:
- navigate [here](https://gitlab.com/u0078867/py3dfreehandus/-/tree/master/samples)
- click on the download button on the top right (arrow down)
- click on *Download this directory*

### Versioned method

Data files and example scripts are tracked via [Git LFS](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html). To get the example data:

*Windows*:

- download and install [git](https://git-scm.com/). During installation, you can leave the default checkboxes as they are. Make sure that Git LFS is in the list of components to install.
- create a profile on [GitLab](https://gitlab.com/), if you don't have it already.
- create an empty folder where you want data to be and inside right-click on **Git Bash here**. To download the sample data for the first time, type:

  ```
  git init
  git remote add origin https://gitlab.com/u0078867/py3dfreehandus.git
  git pull origin master
  pause
  ```

  When pulling, you may be requested username and password of your GitHub account. This operation will download both raw library code and samples data; the sample data is under to folder *samples*.

-  Every time you are notified for sample data updates, double-click on *download_sample_data.bat* (created when pulling) to update your local copy.
