Requirements
============

These are the requirements (in parenthesis, version number tested):

- Python 2 or 3 (3 suggested)
- numpy (1.18.5)
- scipy (1.6.0)
- matplotlib (3.3.4)
- libpython (any version)
- cython (0.29)
- vtk (8.2.0)
- sympy (1.8)
- simpleitk (2.0.2)
- opencv (3.4.9)
- moviepy (1.0.1, **ffdshow** codecs required)
- pydicom (2.1.2)
- btk (0.4)
- guidata (1.7.6)
- scikit-image (0.18.2)
- MinGW64 (4.8.0)

Python and dependencies can be installed in other ways then suggested, but this can easily be
tedious, and hence not suggested.

Py3DFreeHandUS is Python 2 and 3 compatible. However, most of the library versions
needed are not avialable for Python 2 anymore.

In the very likely case the ultrasound files are large (GBytes), it is **strongly** suggested to use a 64-bit Python distribution.

We carefully paid *much* attention about critical aspects such as vectorization, preallocation, RAM consumption,...
to reach realistic computation time on commodity hardware, we still suggest to use a machine with **1.6+ GHz and 4+ GB RAM**.
However, this mainly depends on the factors desribed in :ref:`when-mem-error`.

The tool was tested with data recorded by the following systems:

- US: `Telemed <http://www.telemedultrasound.com/?lang=en>`_, `ESAOTE <http://www.esaote.it/>`_
- Optoelectronic: `Optitrack <http://www.naturalpoint.com/optitrack/>`_, `Vicon <http://vicon.com/>`_

Theoretically, *any* US or optoelectronic device able to export DICOM and C3D files can be used.
We suggest both devices to be **hardware-synchronized** for start and stop acquisition triggers. However, we provided time delay estimation and compensation techniques.

The file formats tested are:

- US: **uncompressed** DICOM. Being uncompressed is a must, since at the moment ``pydicom`` doesn't support compressed formats. We used `MeVisLab <http://www.mevislab.de/>`_ for this.
- Optoelectronic: C3D.

Normally, US systems are also able to export images sequences in AVI format. When compressed, these files are much smaller than uncompressed DICOM (or AVI) files. But, depending on the US machine,
compression can be *lossy*, not ideal for high-quality 3D morphology reconstruction. Plus, AVI files cannot contain mata-data like *Frame Time Vector*, necessary for re-synching optoelectronic
data on US data (see method ``process.Process.setDataSourceProperties()``)
