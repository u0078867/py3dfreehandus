# Installation

## Introduction

To ease the installation, it has been decided to distribute a *built package*
in place of a *source package*. This allows to user not to lose time with
building issues such as MinGW installation, Cython compilation, etc.

The package has been built for:

- Windows 64-bit
- Python 3.7 64-bit

It is strongly suggested, for the non-expert user, not to deviate from the
instructions below.

## Python environment

*Windows*:

- install [Miniconda](https://docs.conda.io/en/latest/miniconda.html) (64-bit).
During the installation, if asked, tick the checkbox related to adding ``conda``
to the Windows *PATH* environment (see later).
Usually, for employer-managed computers, admin restrictions can exist.
If you experience issues with the ``conda`` command (see later), then install
Miniconda inside a user fully manageable folder (e.g. *My Documents*).

- open a Windows console (see [here](https://www.businessinsider.com/how-to-open-command-prompt?r=US&IR=T)
on how to do that).

- make sure that the command ``conda`` is available in the Windows console.
Type ``conda`` in the open console. If the command is not found,
append to the current *PATH* the *Scripts* path (inside miniconda, e.g.: *C:\Users\your_user\miniconda3\Scripts*). For conda version >= 4.6, append also *C:\Users\your_user\miniconda3\condabin*, but **BEFORE** the *Scripts* path.
See [here](https://helpdeskgeek.com/windows-10/add-windows-path-environment-variable/)
how to append strings to the *PATH* variable.
This operation is performed automatically during the installation of Miniconda,
if the user ticks the dedicated checkbox (see earlier).

- If you needed to adjust the *PATH* manually, close the previusly open
Window console and open a new one.

- type the following to initialize the conda tool:

    ```
    conda init
    ```

- type the following to create the dedicated Python environment (and confirm):

    ```
    conda create -n py3dfus -c conda-forge python=3.7.10 numpy=1.18.5 scipy=1.6.0 matplotlib=3.3.4 libpython cython==0.29 vtk=8.2.0 sympy=1.8 simpleitk=2.0.2 opencv=3.4.9 moviepy=1.0.1 pydicom=2.1.2 btk=0.4 guidata=1.7.6 scikit-image=0.18.2 spyder=3.3.6
    ```

- activate the dedicated environment:

    ```
    activate py3dfus
    ```

- install Py3DFreeHandUS:

    ```
    pip install Py3DFreeHandUS --no-cache-dir
    ```

- check that the library is well importable:

    ```
    python
    ...
    >>> import Py3DFreeHandUS
    ```

## Spyder

Spyder is one Python editor that incorporates Python consoles as well.

To run Spyder for the sandboxed environment, open a window console and type
the following content:

```
activate py3dfus
spyder
```

and double-click on it.

Alternatively, Spyder can be found among the installed software in your
operative system.

To configure Spyder, in *Windows*:

- go to *Run*, in *Console*, select *Execute in current console*.
- go to *Tools, Preferences, IPython console, Graphics* and uncheck *Activate support*.
- go to Python installation folder, navigate to */envs/3dfus/Lib/site-packages/matplotlib/mpl-data/* and then open the file *matplotlibrc* with any text editor.
- around line 41, replace **backend      : TkAgg** with **backend      : Qt4Agg**. This should make IPython behave like the old classical Python console, in terms of charts.

All these configurations above are avoided in case Spyder is only used for
*editing* code, and not for *running* it. Running code from console is safer.

**NOTE**: for the good behaviour of matplotlib charts, it is advised to close the IPython console after the script has terminated. A new fresh one will be automatically created.

**IMPORTANT NOTE**: this library is under active development.
We do our best to try to maintain compatibility with previous versions.
