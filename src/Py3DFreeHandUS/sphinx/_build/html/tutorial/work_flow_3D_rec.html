
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Workflow for 3D volume reconstruction &#8212; Py3DFreeHandUS 1.0 documentation</title>
    <link rel="stylesheet" type="text/css" href="../_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="../_static/classic.css" />
    
    <script data-url_root="../" id="documentation_options" src="../_static/documentation_options.js"></script>
    <script src="../_static/jquery.js"></script>
    <script src="../_static/underscore.js"></script>
    <script src="../_static/doctools.js"></script>
    <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script>window.MathJax = {"options": {"processHtmlClass": "tex2jax_process|mathjax_process|math|output_area"}}</script>
    
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Code/data samples" href="code_samples.html" />
    <link rel="prev" title="Tutorial" href="tutorial.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="code_samples.html" title="Code/data samples"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="tutorial.html" title="Tutorial"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="../index.html">Py3DFreeHandUS 1.0 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="tutorial.html" accesskey="U">Tutorial</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Workflow for 3D volume reconstruction</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="workflow-for-3d-volume-reconstruction">
<h1>Workflow for 3D volume reconstruction<a class="headerlink" href="#workflow-for-3d-volume-reconstruction" title="Permalink to this headline">¶</a></h1>
<p>The following chapter <strong>summarizes</strong> the most important concepts described in articles <a class="reference internal" href="../intro.html#ref1" id="id1"><span>[Ref1]</span></a>, <a class="reference internal" href="../intro.html#ref2" id="id2"><span>[Ref2]</span></a>, <a class="reference internal" href="../intro.html#ref3" id="id3"><span>[Ref3]</span></a>.
It is <strong>strongly recommended</strong> to read those first.</p>
<p>Briefly, there are two different measurement systems involved:</p>
<p>1. US (<em>UltraSound</em>) system: this system, based on ultra-sounds reflexion, is able to acquire 2D images regarding
subcutaneous body structures including tendons, muscles, joints, vessels and internal organs. This system is
normally composed by a <strong>probe</strong>, containing the sensors able to record reflected ultrasounds. The pixels of each image
can be expressed in the <strong>scan reference frame P</strong>. Normally, all the acquired images are stored into <strong>DICOM</strong> files.</p>
<p>2. Marker-based optoelectronic system (<em>opto</em>): an ensemble of cameras capturing, at the same time, the 3D position of
(active or passive) <strong>markers</strong>. The position is expressed with respect to a <strong>global reference frame T</strong>. The positions
of the markers is normally stored in a binary <strong>C3D</strong> file.</p>
<p>The start-acquisition and stop-acquisition triggers for both devices are supposed to be <strong>hardware-synchronized</strong>.
The two systems have different <strong>acquisition frequencies</strong>, normally the opto system one being higher.</p>
<p>By putting some markers on the US probe, it is possible to construct a <strong>probe reference frame R</strong>.
Thus, it is possible to know, for very time frame of the combined data acquisition, the <strong>attitude</strong>
(rotation + translation) of the US probe with respect to <strong>T</strong>. Since the position of the 2D scans
with respect to <strong>R</strong> can be found by a procedure called <strong>Calibration</strong>, then it also possible to know
the attitude of each 2D scan (and each pixel into it) into <strong>T</strong>.</p>
<p>Let the attitude from frame <strong>A</strong> to <strong>B</strong> be expressed by a 4x4 matrix containing rotation matrix and position vector: <span class="math notranslate nohighlight">\(^{B}T_{A}\)</span>.
Let <span class="math notranslate nohighlight">\(^{H}\bar{x}\)</span> be the pixel coordinates vector in a generic reference frame <strong>H</strong>.</p>
<p>What mentioned above is expressed by the following equation: <span class="math notranslate nohighlight">\(^{T}\bar{x}\ =\ ^{T}T_{R}\ ^{R}T_{P}\ ^{P}\bar{x}\)</span>.</p>
<p>These are the steps performed during a full processing session:</p>
<section id="devices-delay-estimation-and-compensation">
<h2>Devices delay estimation and compensation<a class="headerlink" href="#devices-delay-estimation-and-compensation" title="Permalink to this headline">¶</a></h2>
<p>Although we strongly suggest to hardware-trigger both devices with a common start-stop acquisition trigger, this is not always
possible or anyway there is still a time delay to be compensated later. We provide the possibility to estimate and compensate
this time delay.</p>
</section>
<section id="probe-spatial-calibration">
<h2>Probe spatial calibration<a class="headerlink" href="#probe-spatial-calibration" title="Permalink to this headline">¶</a></h2>
<p>This is aimed at calculating <span class="math notranslate nohighlight">\(^{R}T_{P}\)</span> (see <a class="reference internal" href="../intro.html#ref2" id="id4"><span>[Ref2]</span></a>). Briefly, it is based on solving a system of equations similar to the
one above (by expressing <span class="math notranslate nohighlight">\(^{T}\bar{x}\)</span> in a more convenient <strong>calibration phantom reference frame C</strong>) and imposing
<span class="math notranslate nohighlight">\(^{C}\bar{x}\)</span> to respect some constraints.</p>
<p>Below a schematic overview of the calibration protocol. US probe is scanning the bottom of a water tank. Here, the constraint is
that, for all the US image, that a the vertical coordinate of <span class="math notranslate nohighlight">\(^{C}\bar{x}\)</span> is always 0 for all the time frames.</p>
<a class="reference internal image-reference" href="../_images/probe_calib.png"><img alt="../_images/probe_calib.png" src="../_images/probe_calib.png" style="width: 768.0px; height: 500.0px;" /></a>
</section>
<section id="calibration-quality-assessment">
<h2>Calibration quality assessment<a class="headerlink" href="#calibration-quality-assessment" title="Permalink to this headline">¶</a></h2>
<p>This is the process of estimating both <strong>precision</strong> and <strong>accuracy</strong> of the calibration phase. <strong>Precision</strong> gives an indication
of the dispersion of measures around their mean. <strong>Accuracy</strong> gives an indication of the difference between the mean of the measures
and the real value. For details, read <a class="reference internal" href="../intro.html#ref1" id="id5"><span>[Ref1]</span></a>. This measure can be, for instance, the known position of a point in space (<em>Point accuracy</em>)
or the known dimension of an object (<em>Distance accuracy</em>).</p>
</section>
<section id="d-voxel-reconstruction">
<h2>3D Voxel reconstruction<a class="headerlink" href="#d-voxel-reconstruction" title="Permalink to this headline">¶</a></h2>
<p>Here, the 2D US scans are “aligned” in the 3D space by using the equation above. A <strong>3D voxel-array</strong> is created, containing
the grey values of all the repositioned original pixels. The voxel-array (a parallelepipedon) should be the smallest one containing
the sequence of realigned scans, in order to avoid RAM waste.</p>
</section>
<section id="gaps-filling">
<h2>Gaps filling<a class="headerlink" href="#gaps-filling" title="Permalink to this headline">¶</a></h2>
<p>After all the scans are correctly positioned in the 3D space, there are inevitably “gaps” in the voxel-array, i.e., voxels for which
the grey value is unknown. The quick-and-dirty way, known as <strong>VNN</strong> (<em>Voxel Nearest Neighbour</em>), consists of filling a gap by using
the closest voxel having an assigned grey value. But other more sophisticated techniques are available.</p>
</section>
<section id="further-analysis">
<h2>Further analysis<a class="headerlink" href="#further-analysis" title="Permalink to this headline">¶</a></h2>
<p>The gaps-filled voxel-array can be served for other further analysis, such as <strong>features extraction</strong> like <strong>body structure border extraction</strong>
and <strong>volume calculation</strong>.</p>
<p>Below, we show the result of manual border identification and surface mesh reconstruction of a human calf muscle (by using <a class="reference external" href="http://www.mevislab.de/">MeVisLab</a>):</p>
<img alt="../_images/3d_mesh_muscle.png" src="../_images/3d_mesh_muscle.png" />
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="../index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Workflow for 3D volume reconstruction</a><ul>
<li><a class="reference internal" href="#devices-delay-estimation-and-compensation">Devices delay estimation and compensation</a></li>
<li><a class="reference internal" href="#probe-spatial-calibration">Probe spatial calibration</a></li>
<li><a class="reference internal" href="#calibration-quality-assessment">Calibration quality assessment</a></li>
<li><a class="reference internal" href="#d-voxel-reconstruction">3D Voxel reconstruction</a></li>
<li><a class="reference internal" href="#gaps-filling">Gaps filling</a></li>
<li><a class="reference internal" href="#further-analysis">Further analysis</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="tutorial.html"
                        title="previous chapter">Tutorial</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="code_samples.html"
                        title="next chapter">Code/data samples</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../_sources/tutorial/work_flow_3D_rec.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="../genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="../py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="code_samples.html" title="Code/data samples"
             >next</a> |</li>
        <li class="right" >
          <a href="tutorial.html" title="Tutorial"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="../index.html">Py3DFreeHandUS 1.0 documentation</a> &#187;</li>
          <li class="nav-item nav-item-1"><a href="tutorial.html" >Tutorial</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Workflow for 3D volume reconstruction</a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2021, Davide Monari.
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 4.1.1.
    </div>
  </body>
</html>