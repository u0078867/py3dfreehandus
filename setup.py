# -*- coding: utf-8 -*-


from setuptools import setup, find_packages
from Cython.Build import cythonize
from distutils.extension import Extension
import os, sys
import numpy


ext_modules = [
    Extension('Py3DFreeHandUS.image_utils_c', ['src/Py3DFreeHandUS/image_utils_c.pyx']),
    Extension('Py3DFreeHandUS.calib_c', ['src/Py3DFreeHandUS/calib_c.pyx'])
]


setup(
    name='Py3DFreeHandUS',
    version='1.0.0',
    description='Library for 3D free-hand ultrasonography',
    long_description='See: https://gitlab.com/u0078867/py3dfreehandus',
    packages=find_packages('src'),
    package_dir={'':'src'},
    package_data={
        'Py3DFreeHandUS': [
            '*.pyx',
            '*.pxd',
            'LICENSE.txt',
        ]
    },
    include_package_data=True,
    ext_modules=cythonize(ext_modules, annotate=True),
    include_dirs=[numpy.get_include(), os.path.join(numpy.get_include(), 'numpy')],
    license='MIT',
    author='u0078867',
    author_email='davide.monari@kuleuven.be',
    url='https://gitlab.com/u0078867/py3dfreehandus',
)
