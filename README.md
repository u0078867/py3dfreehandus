
**A Python library for 3D free-hand ultra-sound measurements**.

Folder *Py3DFreeHandUS* contains source code and doc.

HTML doc is in *sphinx/_build/html/index.html* or visit https://u0078867.gitlab.io/py3dfreehandus/
